package ro.fortech.hello;

import static java.util.Calendar.DAY_OF_WEEK;
import static java.util.Calendar.LONG_FORMAT;
import static java.util.Locale.US;

import java.util.Calendar;

class Result {

	public static String findDay(int month, int day, int year) {
		Calendar calendar = new Calendar.Builder().setDate(year, month - 1, day).build();
		return calendar.getDisplayName(DAY_OF_WEEK, LONG_FORMAT, US).toUpperCase();
	}
}

package ro.fortech.hello;

import static ro.fortech.hello.Result.findDay;

public class MainClass {

	public static void main(String[] args) {
		int day = 13;
		int month = 8;
		int year = 2019;
		System.out.println(findDay(month, day, year));
	}
}
